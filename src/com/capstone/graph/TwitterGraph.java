package com.capstone.graph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class TwitterGraph implements Graph {
	HashMap<Integer, TwitterNode> nodes;
	
	public TwitterGraph(){
		nodes = new HashMap<Integer, TwitterNode>();
	}
	
    /* Creates a vertex with the given number. */
    public void addVertex(int num) {
    	if (!nodes.containsKey(num))
    		nodes.put(Integer.valueOf(num), new TwitterNode(Integer.valueOf(num)));
    }
    
    /* Creates a vertex with a TwitterNode. */
    public void addVertex(TwitterNode n) {
    	nodes.putIfAbsent(n.getUid(), n);
    }
    
    /* Return all nodes. */
    public HashMap<Integer, TwitterNode> getNodes() {
    	return nodes;
    }
    
    /* Creates a follower relation. */
    public void addEdge(int from, int to) {
    	if (!nodes.containsKey(from)) {
    		addVertex(from);
    	} 
    	if (!nodes.containsKey(to)) {
    		addVertex(to);
    	}
    	//
//    	nodes.get(from).addRelation(nodes.get(to), "following");
    	nodes.get(to).addRelation(nodes.get(from), "follower");
    }
    
    /* Creates a retweeter relation. */
    public void addRTEdge(int from, int to) {
    	if (!nodes.containsKey(from)) {
    		addVertex(from);
    	} 
    	if (!nodes.containsKey(to)) {
    		addVertex(to);
    	}
//    	nodes.get(from).addRelation(nodes.get(to), "retweeted");
    	nodes.get(to).addRelation(nodes.get(from), "retweetedby");
    }
    
    /* Set all nodes in the graph to unvisited. */
    public void resetVisited() {
    	for (Integer key : nodes.keySet()) {
    		nodes.get(key).setVisited(false);
    	}
    }
    
    /* Set all nodes in toVisit set to visited. */
    public void visitAll(Set<TwitterNode> toVisit) {
    	for (TwitterNode t : toVisit) {
    		t.setVisited(true);
    	}
    }
    
    /* Find a subgraph that includes the start point, and covers at least size of nodes
     * that involves nodes with at few followers as possible. */
    public Graph grassrootPath(Integer start, Integer size) {
    	TwitterGraph path = new TwitterGraph();
    	if (size > nodes.size()) {
    		System.out.println("Do not have enough nodes in graph to visit.");
    		return path;
    	}
    	Comparator<TwitterNode> c = (x,y) -> Integer.compare(x.sizeOfRelation("follower"), y.sizeOfRelation("follower"));
    	PriorityQueue<TwitterNode> toRetweet = new PriorityQueue<TwitterNode>(c);
    	Set<TwitterNode> toBroadcast = new HashSet<TwitterNode>();
    	Set<TwitterNode> visited = new HashSet<TwitterNode>();
    	System.out.println(nodes.get(start));
    	visited.add(nodes.get(start));
    	visitAll(visited);
    	
    	HashMap<Integer, TwitterNode> followers = nodes.get(start).getRelation("follower");
    	HashMap<Integer, TwitterNode> retweetedby = nodes.get(start).getRelation("retweetedby");
    	toRetweet.addAll(retweetedby.values());
    	toBroadcast.addAll(followers.values());
    	visitAll(visited);
    	visited.addAll(toBroadcast);
    	while (visited.size() <= size && toRetweet.size() > 0) {
    		TwitterNode nextNode = toRetweet.poll();
    		System.out.println(nextNode.toString() + " with " + Integer.toString(nextNode.getRelation("follower").size())+ " followers");
    		followers = nextNode.getRelation("follower");
        	retweetedby = nextNode.getRelation("retweetedby");
        	toRetweet.addAll(retweetedby.values());
        	toBroadcast.addAll(followers.values());
        	toBroadcast.add(nextNode);
        	visitAll(visited);
        	visited.addAll(toBroadcast);
    	}
    	for (TwitterNode n : visited)
    		path.addVertex(n);
    	if (visited.size() >= size)
    		System.out.println("Visited " + Integer.toString(visited.size()) + " nodes.");
    	else
    		System.out.println("Cannot reach specified number of nodes.");
    	return path;
    }
    
    /* Set cover approximation. */
    public List<TwitterNode> setCover(TwitterGraph g, String type) {
    	List<TwitterNode> mds = new ArrayList<TwitterNode>();
    	Comparator<TwitterNode> c = (x,y) -> Integer.compare(y.sizeOfUnvisitedRelation(type), x.sizeOfUnvisitedRelation(type));
    	PriorityQueue<TwitterNode> toVisit = new PriorityQueue<TwitterNode>(c);
    	toVisit.addAll(nodes.values());
    	while (!toVisit.isEmpty()) {
    		TwitterNode topnode = toVisit.poll();
    		System.out.println("In MDS: " + topnode.toString() + ", with priority: " + Integer.toString(topnode.sizeOfUnvisitedRelation(type)));
    		mds.add(topnode);
    		HashSet<TwitterNode> visitset = new HashSet<TwitterNode>();
    		visitset.addAll(topnode.getRelation(type).values());
    		visitAll(visitset);
    		toVisit.removeAll(visitset);
    	}
    	return mds;
    }
 
    
    /* Return the graph's connections in a readable format. 
     * The keys in this HashMap are the vertices in the graph.
     * The values are the nodes that are reachable via a directed
     * edge from the corresponding key. 
	 * The returned representation ignores edge weights and 
	 * multi-edges.  */
    public HashMap<Integer, Set<Integer>> exportGraph() {
    	HashMap<Integer, Set<Integer>> export = new HashMap<Integer, Set<Integer>>();
    	for (Integer v : nodes.keySet()) {
    		export.put(v, nodes.get(v).getRelation("follower").keySet());
    	}
    	return export;
    }
    
    public static void main(String[] args){
    	TwitterGraph g = new TwitterGraph();
//    	GraphLoader.loadGraph(g, "data/testnetwork.txt");
//    	GraphLoader.loadRTGraph(g, "data/testrt.txt");
    	System.out.println("Reading graph 1");
    	GraphLoader.loadGraph(g, "../data/higgs-social_network.txt");
    	System.out.println("Reading graph 2");
    	GraphLoader.loadRTGraph(g, "../data/higgs-retweet_network.txt");
    	
    	List<TwitterNode> mds = g.setCover(g, "retweetedby");
    	
    	System.out.println("\n========");
    	
    	Graph gr = g.grassrootPath(5795, 1000);

    }

}
