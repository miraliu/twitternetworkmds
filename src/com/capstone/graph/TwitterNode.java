package com.capstone.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class TwitterNode {
	private Integer uid;
	private HashMap<String, HashMap<Integer, TwitterNode>> relations;
	private boolean visited;
	int toCompare;
	
	/* Initialize a twitter node. */
	public TwitterNode(Integer id) {
		uid = id;
		visited = false;
		relations = new HashMap<String, HashMap<Integer, TwitterNode>>();
		relations.put("following", new HashMap<Integer, TwitterNode>());
		relations.put("follower", new HashMap<Integer, TwitterNode>());
		relations.put("retweeted", new HashMap<Integer, TwitterNode>());
		relations.put("retweetedby", new HashMap<Integer, TwitterNode>());
	}
	
	
	/* Helper function for adding relation. */
	private boolean addToList(TwitterNode i, HashMap<Integer, TwitterNode> l) {
		if (l.containsKey(i.getUid())) {
			return false;
		} else {
			l.put(i.getUid(), i);
			return true;
		}
	}
	
	/* Return user id. */
	public Integer getUid() {
		return this.uid;
	}
	
	/* Return visit status. */
	public boolean visited() {
		return this.visited;
	}
	
	/* Set node to visited/unvisited. */
	public void setVisited(boolean v) {
		this.visited = v;
	}
	
	/* Add a relation. */
	public boolean addRelation(TwitterNode i, String type) {
		if (relations.containsKey(type)) {
			addToList(i, relations.get(type));
		}
		else
			System.out.println("Error add relation: Relation type invalid.");
			return false;
	}
	
	/* Get a list this node's neighbors of a certain type. */
	public HashMap<Integer, TwitterNode> getRelation(String type) {
		if (relations.containsKey(type)) {
			return relations.get(type);
		}
		else {
			System.out.println("Error getting relation: Relation type invalid.");
			return null;
		}
	}
	
	/* Replace a relation. */
	public void setRelation(String type, HashMap<Integer, TwitterNode> newrelation) {
		if (relations.containsKey(type)) {
			relations.get(type).clear();
			relations.get(type).putAll(newrelation);
		}
		else {
			System.out.println("Error setting relation: Relation type invalid.");
		}
	}
	
	/* Make a copy of the node's relation. */
	public HashMap<Integer, TwitterNode> copyRelation(String type) {
		HashMap<Integer, TwitterNode> copy = new HashMap<Integer, TwitterNode>();
		if (relations.containsKey(type)) {
			copy.putAll(relations.get(type));
		}
		else {
			System.out.println("Error copying relation: Relation type invalid.");
			return null;	
		}
		return copy;
	}
	
	/* Duplicate this node. */
	public TwitterNode duplicate() {
		TwitterNode node = new TwitterNode(this.uid);
		for (String type : relations.keySet()) {
			node.setRelation(type, this.getRelation(type));
		}
		return node;
	}
	
	/* Return size of a relation. */
	public Integer sizeOfRelation(String type) {
		if (relations.containsKey(type)) {
			return relations.get(type).size();
		}
		else {
			System.out.println("Error getting relation size: Relation type invalid.");
			return -1;	
		}
	}
	
	/* Check if the node and node i has a relation of type. */
	public boolean checkRelation(Integer i, String type) {
		if (relations.containsKey(type)) {
			return relations.get(type).containsKey(i);
		}
		else {
			System.out.println("Error checking relation: Relation type invalid.");
			return false;	
		}
	}
	
	/* Visit all nodes in a relation. */
	public Integer visitRelation(String type) {
		HashMap<Integer, TwitterNode> toVisit = new HashMap<Integer, TwitterNode>();
		if (relations.containsKey(type)) {
			toVisit = relations.get(type);
		}
		else {
			System.out.println("Error checking relation: Relation type invalid.");
			return -1;	
		}
		int i = 0;
		for (Integer n : toVisit.keySet()) {
			if (!toVisit.get(n).visited()) {
				toVisit.get(n).setVisited(true);
				i++;
			}
		}
		return Integer.valueOf(i);
	}
	
	/* Return the size of unvisited neighbors in a relation. */
	public Integer sizeOfUnvisitedRelation(String type) {
		HashMap<Integer, TwitterNode> toVisit = new HashMap<Integer, TwitterNode>();
		if (relations.containsKey(type)) {
			toVisit = relations.get(type);
		}
		else {
			System.out.println("Error checking relation: Relation type invalid.");
			return -1;	
		}
		int i = 0;
		if (!this.visited)
			i += 1;
		for (Integer n : toVisit.keySet()) {
			if (!toVisit.get(n).visited()) {
				i++;
			}
		}
		return Integer.valueOf(i);
	}
	
	/* Print node. */
	public String toString() {
		return "Node: " + Integer.toString(uid);
	}

}
